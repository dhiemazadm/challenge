class Car {
    // Property
    float fuelLiter;

    // Property
    String name;

    // Constructor
    Car(String name) {
        // Property assignment
        this.name = name;
    }

    // Method
    void fuelRefill(float liter) {
        // Property addition assignment
        this.fuelLiter += liter;
    }

    // Method
    boolean run() {
        // TODO:
        // - return false if empty fuel
        // - decrease 1 liter per kilometer

        if (fuelLiter < 0) {
            return false;
        }

        fuelLiter -= 1;
        return true;
    }

    // Entrypoint Method
    public static void main(String[] args) throws InterruptedException {
        // Local Variable
        int kmTarget = 10;
        // Instance
        Car miniBus = new Car("minibus");
        // 5 liters fuel
        miniBus.fuelRefill(5);

        // For Loop
        for (int km = 0; km <= kmTarget; km++) {
            // Instance Method Call
            boolean running = miniBus.run();

            // Conditional
            if (!running) {
                // Static Method Call
                System.out.println("fuel empty");

                // Break the loop
                break;
            }

            // Static Method Call
            System.out.println(km + "km");

            // Static Method Call
            Thread.sleep(1000);
        }
    }
}